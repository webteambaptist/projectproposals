﻿namespace ProjectProposalsWeb.Models
{
    public class NewRequest
    {
        public Fields fields { get; set; }
    }

    public class Fields
    {
        public Project project { get; set; }
        public Parent parent { get; set; }
        public string summary { get; set; }
        public Description description { get; set; }
        public IssueType issuetype { get; set; }
    }

    public partial class Project
    {
        public string key { get; set; }
    }

    public partial class Parent
    {
        public string key { get; set; }
    }

    public partial class Description
    {
        public string type { get; set; }
        public int version { get; set; }
        public IList<Content> content { get; set; }
    }

    public partial class Content
    {
        public string type { get; set; }
        public IList<_Content> content { get; set; }
    }

    public partial class IssueType
    {
        public string name { get; set; }
    }

    public partial class _Content
    {
        public string type { get; set; }
        public string text { get; set; }
    }
}
