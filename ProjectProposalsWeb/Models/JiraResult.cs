﻿namespace ProjectProposalsWeb.Models
{
    public class JiraResult
    {
        public string id { get; set; }
        public string key { get; set; }
        public string self { get; set; }
    }
}
