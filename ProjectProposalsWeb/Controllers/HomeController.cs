﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using ProjectProposalsWeb.Models;
using ProjectProposalsWeb.Services;
using System.Diagnostics;

namespace ProjectProposalsWeb.Controllers
{
    public class HomeController : Controller
    {
        private readonly LoggerService _loggerService;
        private readonly AdService _adService;
        private readonly ListItemService _listItemService;
        private readonly DataService _dataService;
        private readonly MailService _mailService;
        private IConfiguration _config;

        private readonly string _user;

        public HomeController(IConfiguration config, IHttpContextAccessor accessor)
        {
            _config = config;
            _loggerService = new LoggerService("Home");
            _adService = new AdService(accessor);
            _listItemService = new ListItemService(config);
            _dataService = new DataService(config);
            _mailService = new MailService(config);

            var user = _adService.GetUserName();
            _user = _adService.CleanUsername(user);
        }

        /// <summary>
        /// Loads main page of application
        /// </summary>     
        /// <returns>View</returns>
        public async Task<IActionResult> Index()
        {           
            try
            {
                await LoadSelectLists();

                ViewData["User"] = _user;

                var userEmail = _user + "@bmcjax.com";
                ViewData["UserEmail"] = userEmail;

                return View();
            }
            catch (Exception e)
            {             
                var errorMsg = $"There was an issue loading the form. :: Details: {e.Message}" + e.InnerException ?? "";

                _loggerService.WriteError($"Exception :: Home::SubmitProposal() :: {errorMsg}:: Details: {e.Message}" + e.InnerException ?? "");

                return RedirectToAction("Error", "Home", new { errorMsg = errorMsg });
            }
        }

        /// <summary>
        /// Loads all the data for the select lists used on the page.
        /// </summary>  
        private async Task LoadSelectLists()
        {
            try
            {
                var types = await _listItemService.GetTypesSelectList();
                ViewData["Types"] = types;

                var priorities = await _listItemService.GetPrioritiesSelectList();
                ViewData["Priorities"] = priorities;
            }
            catch (Exception e)
            {
                var errorMsg = $"Issue retrieving data for the select lists. :: Details: {e.Message}" + e.InnerException ?? "";

                _loggerService.WriteError($"Exception :: Home::LoadPageSelectLists() :: {errorMsg}");
            }
        }

        /// <summary>
        /// Submits New Proposal 
        /// <param name="col">Form Collection Values</param>
        /// </summary>     
        /// <returns>View</returns>
        [HttpPost]
        public async Task<IActionResult> SubmitProposal(IFormCollection col, List<IFormFile> files)
        {
            try
            {
                //Generate Reference Id before submitting new Proposal
                var guid = Guid.NewGuid();
                var referenceId = guid.ToString().Substring(0, 6);

                //Build Out description
                var description = col["description"];
                description += $"\n\n Reference Id: {referenceId}";
                description += $"\n Requestor: {col["userName"]}";
                description += $"\n Requestor Email: {col["userEmail"]}";
                description += $"\n Requestor Contact #: {col["contactNumber"]}";
                description += $"\n Stakeholder: {col["stakeHolder"]}";
                description += $"\n Business Unit: {col["businessUnit"]}";
                description += $"\n Project Proposal Type: {col["types"]}";
                description += $"\n Project Proposal Priority: {col["priorities"]}";

                var newRequest = new NewRequest();
                newRequest = GenerateRequest(col["proposalSubject"].ToString(), description, col["types"].ToString());                    

                //Submit task to Jira itself
                var jiraResults = await _dataService.PostProposalTask(newRequest);

                if (files.Count > 0)
                {
                    await _dataService.PostProposalAttachments(jiraResults, files);
                }

                if (jiraResults != null)
                {
                    //Retrieve Jira task Id
                    var taskId = jiraResults.key;                    

                    //Email
                    _mailService.GenerateEmail(referenceId, col, jiraResults, files);

                    //Redirect user to confirmation page.
                    return RedirectToAction("Confirmation", "Home", new { taskId = taskId, referenceId = referenceId, taskTitle = col["proposalSubject"], taskDescription = col["description"] });
                }
                else
                {
                    var errorMsg = $"Issue occurred submitting Project Proposal.";
                    _loggerService.WriteError($"Exception :: Home::SubmitProposal() :: Details: {errorMsg}");

                    return RedirectToAction("Error", "Home", new { errorMsg = errorMsg });
                }
            }
            catch (Exception e)
            {
                var errorMsg = $"Issue occurred submitting Project Proposal.";

                _loggerService.WriteError($"Exception :: Home::SubmitProposal() :: {errorMsg}:: Details: {e.Message}" + e.InnerException ?? "");

                return RedirectToAction("Error", "Home", new { errorMsg = errorMsg });
            }
        }

        /// <summary>
        /// Submits New Proposal 
        /// <param name="proposalSubject">Subject of proposal/request</param>
        /// <param name="description">Description of proposal/request</param>
        /// <param name="requestType">Type of proposal/request</param>
        /// </summary>     
        /// <returns>NewRequest Model</returns>
        private NewRequest GenerateRequest(string proposalSubject, string description, string requestType)
        {
            //Build json to add item to Jira itself.
            var newRequest = new NewRequest
            {
                fields = new Fields
                {
                    project = new Project
                    {
                        key = _config["AppSettings:JiraProjectKey"]
                    },                    
                    summary = proposalSubject,
                    description = new Description
                    {
                        type = "doc",
                        version = 1,
                        content = new List<Content>
                        {
                            new Content
                            {
                                type = "paragraph",
                                content = new List<_Content>
                                {
                                    new _Content
                                    {
                                        type = "text",
                                        text = description
                                    }
                                }
                            }
                        }
                    }
                }
            };

            switch (requestType)
            {
                case "Issue":
                    newRequest.fields.parent = new Parent
                    {
                        key = "DPR-2"
                    };
                    newRequest.fields.issuetype = new IssueType
                    {
                        name = "Bug"
                    };
                    break;

                case "Enhancement":
                    newRequest.fields.parent = new Parent
                    {
                        key = "DPR-5"
                    };
                    newRequest.fields.issuetype = new IssueType
                    {
                        name = "Story"
                    };
                    break;
                default:
                    newRequest.fields.parent = new Parent
                    {
                        key = "DPR-1"
                    };
                    newRequest.fields.issuetype = new IssueType
                    {
                        name = "Story"
                    };
                    break;
            }

            return newRequest;
        }

        /// <summary>
        /// Confirmation page 
        /// <param name="taskId">Task Id within Jira</param>
        /// <param name="referenceId">Generated Reference Id</param>
        /// <param name="taskTitle">Title of the task within Jira</param>
        /// <param name="taskDescription">Description of the task within Jira</param>
        /// </summary>     
        /// <returns>View</returns>
        public IActionResult Confirmation(string taskId, string referenceId, string taskTitle, string taskDescription)
        {
            try
            {
                ViewData["TaskId"] = taskId;
                ViewData["ReferenceId"] = referenceId;
                ViewData["TaskTitle"] = taskTitle;
                ViewData["TaskDescription"] = taskDescription;

                return View();
            }
            catch (Exception e)
            {
                var errorMsg = $"Issue into database";
                _loggerService.WriteError($"Exception :: Home::PostNewServer() :: {errorMsg}:: Details: {e.Message}" + e.InnerException ?? "");

                return RedirectToAction("Error", "Home", new { errorMsg = errorMsg });
            }            
        }           

        /// <summary>
        /// Error page when something goes wrong retreiving data from db
        /// <param name="errorMsg">Error Msg to display on page</param>
        /// </summary>     
        /// <returns>View</returns>
        public ActionResult Error(string errorMsg)
        {
            ViewData["ErrorMsg"] = errorMsg;
            return View();
        }
    }
}