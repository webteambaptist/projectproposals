﻿using NLog;
using NLog.Config;
using NLog.Targets;
using LogLevel = NLog.LogLevel;

namespace ProjectProposalsWeb.Services
{
    public class LoggerService
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Constructor used to construct and instance of the logger
        /// </summary>
        /// <param name="fileName">The name of the Log to be created</param>
        public LoggerService(string fileName)
        {
            var config = new LoggingConfiguration();
            var logFile = new FileTarget("logfile")
            { FileName = $"Logs\\{fileName}-{DateTime.Today:MM-dd-yy}.log" };
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
            LogManager.Configuration = config;
        }

        /// <summary>
        /// Writes Information to log
        /// </summary>
        /// <param name="text">text to be logged</param>
        public void WriteInfo(string text)
        {
            Logger.Info(text);
        }

        /// <summary>
        /// Writes Error to log
        /// </summary>
        /// <param name="text">text to be logged</param>
        public void WriteError(string text)
        {
            Logger.Error(text);
        }
    }
}
