﻿using Newtonsoft.Json;
using ProjectProposalsWeb.Models;
using System.Net.Http.Headers;
using System.Text;

namespace ProjectProposalsWeb.Services
{
    public class DataService
    {
        private readonly LoggerService _loggerService;
        private readonly IConfiguration _config;

        public DataService(IConfiguration config)
        {
            _config = config;
            _loggerService = new LoggerService("DataService");
        }

        /// <summary>
        /// Submits new Proposal into Jira
        /// </summary>
        /// <param name="newProposal"></param>
        /// <returns>JiraResult Model</returns>
        public async Task<JiraResult> PostProposalTask(NewRequest newRequest)
        {
            try
            {
                var serviceUri = $"{_config["AppSettings:RestUrl"]}/{_config["AppSettings:PostNewIssue"]}";
                var username = _config["AppSettings:Authentication:Username"];
                var pw = _config["AppSettings:Authentication:Password"];

                using var client = new HttpClient();
                var json = JsonConvert.SerializeObject(newRequest);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                var byteArray = Encoding.ASCII.GetBytes($"{username}:{pw}");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

                var response = await client.PostAsync(serviceUri, content);
                var results = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<JiraResult>(results);
            }
            catch (Exception e)
            {
                _loggerService.WriteError($"Exception :: DataService::PostProposalTask({JsonConvert.SerializeObject(newRequest)}) :: {e.Message}" + e.InnerException ?? "");

                return null;
            }
        }

        /// <summary>
        /// Submits attachments for the task just created
        /// </summary>
        /// <param name="jiraResults"></param>
        /// <param name="files"></param>
        public async Task PostProposalAttachments(JiraResult jiraResults, List<IFormFile> files)
        {
            try
            {
                var serviceUri = $"{_config["AppSettings:RestUrl"]}/{_config["AppSettings:PostNewIssue"]}/{jiraResults.key}/attachments";
                var username = _config["AppSettings:Authentication:Username"];
                var pw = _config["AppSettings:Authentication:Password"];

                using var client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Add("X-Atlassian-Token", "no-check");

                var byteArray = Encoding.ASCII.GetBytes($"{username}:{pw}");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

                foreach (var file in files)
                {
                    var multiPartContent = new MultipartFormDataContent("-data-");

                    ByteArrayContent byteArrayContent;

                    using (var ms = new MemoryStream())
                    {
                        await file.CopyToAsync(ms);
                        var fileBytes = ms.ToArray();
                        byteArrayContent = new ByteArrayContent(fileBytes);
                    }

                    multiPartContent.Add(byteArrayContent, "file", file.FileName);

                    await client.PostAsync(serviceUri, multiPartContent);
                }
            }
            catch (Exception e)
            {
                _loggerService.WriteError($"Exception :: DataService::PostProposalAttachments :: {e.Message}" + e.InnerException ?? "");
                throw;
            }
        }
    }
}
