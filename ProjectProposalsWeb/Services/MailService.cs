﻿using ProjectProposalsWeb.Models;
using System.Net.Mail;

namespace ProjectProposalsWeb.Services
{
    public class MailService
    {
        private readonly LoggerService _loggerService;
        private readonly IConfiguration _config;

        public MailService(IConfiguration config)
        {
            _loggerService = new LoggerService("Mail");
            _config = config;
        }      

        /// <summary>
        /// Generate Email Content and Info
        /// <param name="referenceId">Reference Id string</param>
        /// <param name="col">Form Collection Values</param>
        /// <param name="jiraResults">Jira Results generated from submit</param>
        /// </summary>     
        public void GenerateEmail(string referenceId, IFormCollection col, JiraResult jiraResults, List<IFormFile> files)
        {
            try
            {
                var body = "<div style='font-family:calibri'>";
                body += $"<span>Hi {col["userName"]},<br/><br/></span>";
                body += "<span>Thank you for contacting our DevOps team and submitting your Project/Support Request. A team member will reach out to advise Next Steps regarding your request.</span>";
                body += "<br/>";
                body += "<br/>";
                body += "<strong><u>DevOps SLA:</u></strong>";
                body += "<br/>";

                body += "<ul>";
                body += "<li><strong><u>Support Requests:</u></strong> Reviewed Daily. A DevOps PM/PO will contact you within 24 hours to advise the status of your request.</li>";
                body += "<li><strong><u>Project Requests:</u></strong> Reviewed every Friday. A DevOps PM/PO will contact you by end of business Friday, to schedule a Discovery meeting to further discuss your project request.</li>";
                body += "</ul>";
                body += "<br/>";
                body += "<p><strong>Note:</strong> A copy of your request is detailed below for your reference.</p>";
                body += "<p>Thank you, <br/><strong>DevOps Project Support Team</strong></p>";

                body += "<hr/><hr/><p><strong><br/>REQUEST DETAILS:</strong></p>";

                body += "<table border='1' style='width: 500px;'><tbody><tr>";
                body += "<td style='width: 50%;'><span>Request Reference ID</span></td>";
                body += $"<td style='width:50%;'><span>{referenceId}</span><br/><small>You will use this as a reference when contacting our team.</small></td></tr>";
                body += $"<tr><td><span>Jira Task #</span></td><td><span>{jiraResults.key}</span></td></tr>";
                body += $"<tr><td><span>Task Title</span></td><td><span style='white-space: nowrap;'>{col["proposalSubject"]}</a></span></td></tr>";
                body += $"<tr><td><span>Attachments Included in Jira</span></td><td><span style='white-space: nowrap;'>{files.Count}</a></span></td></tr>";
                body += $"<tr><td><span>Description</span></td><td><span style='white-space: nowrap;'>{col["description"]}</span></span></td></tr></tbody></table>";

                body += "<p><strong><br/>REQUESTOR INFORMATION:</strong></p>";

                body += "<table border='1' style='width: 500px;'><tbody><tr>";
                body += $"<tr><td><span>Requestor</span></td><td><span style='white-space: nowrap;'>{col["userName"]}</a></span></td></tr>";
                body += $"<tr><td><span>Requestor Email</span></td><td><span style='white-space: nowrap;'>{col["userEmail"]}</a></span></td></tr>";
                body += $"<tr><td><span>Requestor Contact #</span></td><td><span style='white-space: nowrap;'>{col["contactNumber"]}</a></span></td></tr>";
                body += $"<tr><td><span>Stakeholder</span></td><td><span style='white-space: nowrap;'>{col["stakeHolder"]}</a></span></td></tr>";
                body += $"<tr><td><span>Request Type</span></td><td><span style='white-space: nowrap;'>{col["types"]}</a></span></td></tr>";
                body += $"<tr><td><span>Request Priority</span></td><td><span style='white-space: nowrap;'>{col["priorities"]}</a></span></td></tr>";
                body += $"<tr><td><span>Business Unit</span></td><td><span style='white-space: nowrap;'>{col["businessUnit"]}</a></span></td></tr></tbody></table>";

                body += "</div>";

                var subject = "New Project/Support Request Submitted";
                var to = col["userEmail"];
                var bcc = _config["AppSettings:Mail:MailTo"];

                SendMail(body, to, null, bcc, subject);
            }
            catch (Exception e)
            {
                var errorMsg = $"Issue sending email to requestor for submitted task. :: Details: {e.Message}" + e.InnerException ?? "";

                _loggerService.WriteError($"Exception :: Home::GenerateEmail() :: {errorMsg}");
                throw;
            }
        }

        /// <summary>
        /// This sends email to recipient
        /// </summary>
        /// <param name="body">body of email</param>
        /// <param name="to">recipient</param>
        /// <param name="cc">CC in email</prarm>
        /// <param name="bcc">BCC in email</prarm>
        public void SendMail(string body, string to, string cc, string bcc, string subject)
        {
            try
            {
                var from = _config["AppSettings:Mail:MailFrom"];
                var addressFrom = new MailAddress(from);

                var message = new MailMessage()
                {
                    From = addressFrom,
                    Body = body,
                    IsBodyHtml = true,
                    Subject = subject
                };

                if (to.Contains(";"))
                {
                    var splitTo = to.Split(";");
                    foreach (var emailAddress in splitTo)
                    {
                        message.To.Add(emailAddress);
                    }
                }
                else
                {
                    message.To.Add(to);
                }

                if (cc != null)
                {
                    message.CC.Add(cc);
                }

                if (bcc != null)
                {
                    message.Bcc.Add(bcc);
                }

                var smtp = new SmtpClient
                {
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Host = "smtp.bmcjax.com",
                    Port = 25,
                    EnableSsl = false
                };
                smtp.Send(message);
            }
            catch (Exception ex)
            {
                _loggerService.WriteError($"Exception :: MailService.SendMail() occured :: Details :: {ex.Message}" + ex.InnerException ?? "");
            }
        }
    }
}
