﻿using Microsoft.AspNetCore.Mvc.Rendering;

namespace ProjectProposalsWeb.Services
{
    public class ListItemService
    {
        private readonly LoggerService _loggerService;
        private readonly IConfiguration _config;

        public ListItemService(IConfiguration config)
        {
            _config = config;
            _loggerService = new LoggerService("ListItems");           
        }

        /// <summary>
        /// Gets all Types from appsettings config file.
        /// </summary>     
        /// <returns>List of Selectable List Items</returns>
        public async Task<List<SelectListItem>> GetTypesSelectList()
        {
            try
            {
                var types = _config["AppSettings:DropDowns:Types"].Split(',');

                var selectTypes = new List<SelectListItem>();

                var initialListItem = new SelectListItem()
                {
                    Text = "Select Type",
                    Value = ""
                };
                selectTypes.Add(initialListItem);

                foreach (var type in types)
                {
                    var listItem = new SelectListItem()
                    {
                        Text = type,
                        Value = type
                    };
                    selectTypes.Add(listItem);
                }
                return selectTypes;
            }
            catch (Exception e)
            {
                _loggerService.WriteError($"Exception :: ListItemService::GetTypesSelectList() :: Details: {e.Message}" + e.InnerException ?? "");
                return null;
            }
        }

        /// <summary>
        /// Gets all Priorities from appsettings config file.
        /// </summary>     
        /// <returns>List of Selectable List Items</returns>
        public async Task<List<SelectListItem>> GetPrioritiesSelectList()
        {
            try
            {
                var priorities = _config["AppSettings:DropDowns:Priorities"].Split(',');

                var selectPriorities = new List<SelectListItem>();

                var initialListItem = new SelectListItem()
                {
                    Text = "Select Priority",
                    Value = ""
                };
                selectPriorities.Add(initialListItem);

                foreach (var priority in priorities)
                {
                    var listItem = new SelectListItem()
                    {
                        Text = priority,
                        Value = priority
                    };
                    selectPriorities.Add(listItem);
                }
                return selectPriorities;
            }
            catch (Exception e)
            {
                _loggerService.WriteError($"Exception :: ListItemService::GetPrioritiesSelectList() :: Details: {e.Message}" + e.InnerException ?? "");
                return null;
            }
        }
    }
}
