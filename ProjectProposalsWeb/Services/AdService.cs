﻿using Microsoft.AspNetCore.Authentication;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;

namespace ProjectProposalsWeb.Services
{
    public class AdService
    {
        private readonly ClaimsPrincipal _claim;
        private readonly IHttpContextAccessor _accessor;
        private readonly LoggerService _loggerService;
        private readonly string _userName;
        public AdService(IHttpContextAccessor httpContextAccessor)
        {
            _accessor = httpContextAccessor;
            _claim = httpContextAccessor.HttpContext?.User;
            _loggerService = new LoggerService("AdService");
            _userName = GetUserName();
        }
        /// <summary>
        /// Returns the IPrincipal for the logged in user
        /// </summary>
        public IPrincipal CurrentUser
        {
            get
            {
                var context = _accessor.HttpContext;
                return context != null ? context.User : ClaimsPrincipal.Current;
            }
        }
        /// <summary>
        /// Determines if the logged in user is in the role specified
        /// </summary>
        /// <param name="domain">domain</param>
        /// <param name="user">username</param>
        /// <param name="adGroupName">Ad Group</param>
        /// <returns>bool (yes/no) in role</returns>
        public bool GetAdAccess(string domain, string user, string adGroupName)
        {
            try
            {
#pragma warning disable CA1416 // Validate platform compatibility
                var pc = new PrincipalContext(ContextType.Domain, domain);

                var src = UserPrincipal.FindByIdentity(pc, user)?.GetGroups(pc);


                if (src != null)
                {
                    var isUserInGroup = src.Select(x => x).Select(x => x.Name).Where(y => y.Contains(adGroupName)).ToList();

                    return isUserInGroup.Any();
                }
#pragma warning restore CA1416 // Validate platform compatibility
                return false;
            }
            catch (Exception e)
            {
                _loggerService.WriteError($"Exception :: ADService::GetADAccess({domain}:{user}:{adGroupName}) :: {e.Message}");
                return false;
            }
        }

        /// <summary>
        /// Removes domain and slashes from username
        /// </summary>
        /// <param name="user">Logged in user</param>
        /// <returns></returns>
        public string CleanUsername(string user)
        {
            try
            {
                var findSlash = user.IndexOf("\\", StringComparison.Ordinal);
                user = user.Substring(findSlash + 1);
                return user.ToUpper();
            }
            catch (Exception e)
            {
                _loggerService.WriteError($"Exception :: AdService::CleanUsername({user}) :: {e.Message}");
                return null;
            }
        }
        public bool IsAdmin()
        {
            var webTeam = GetAdAccess("BH", _userName, "BHSQL Web Team Developers");
            return webTeam;
        }
        /// <summary>
        /// Gets logged in user
        /// </summary>
        /// <returns>username</returns>
        public string GetUserName()
        {
            try
            {
                return _accessor.HttpContext?.User.Identity?.Name;
            }
            catch (Exception e)
            {
                _loggerService.WriteError("Exception :: GetUserName occurred :: Details :: " + e.Message + " " + e.InnerException ?? "");
                throw;
            }
        }
        /// <summary>
        /// Get full name for user
        /// </summary>
        /// <param name="userId">logged in user's user id</param>
        /// <returns></returns>
        public string GetFullName(string userId)
        {
            try
            {
                var filter = new StringBuilder();
                filter.Append($"(sAMAccountName={userId})");
#pragma warning disable CA1416 // Validate platform compatibility
                var search = new DirectorySearcher(new DirectoryEntry())
                {
                    PageSize = 1000,
                    Filter = $"(&(objectClass=user)(objectCategory=person)(|{filter}))"
                };

                search.PropertiesToLoad.Add("displayname");
                search.PropertiesToLoad.Add("givenName");

                var result = search.FindOne();
#pragma warning restore CA1416 // Validate platform compatibility
                var managerName = GetProperty(result, "displayname");
                return managerName;
            }
            catch (Exception e)
            {
                _loggerService.WriteError("Exception :: IsUserPermitted :: UserID: " + userId + " occurred :: Details :: " + e.Message + " " + e.InnerException ?? "");
                throw;
            }

        }
        /// <summary>
        /// This pulls out the actual value from the AD property passed in
        /// </summary>
        /// <param name="searchResult">Search Result passed in that contains the properties specified</param>
        /// <param name="propertyName">Property searching</param>
        /// <returns></returns>
        public string GetProperty(SearchResult searchResult, string propertyName)
        {
            try
            {
#pragma warning disable CA1416 // Validate platform compatibility
                return searchResult.Properties.Contains(propertyName)
                    ? searchResult.Properties[propertyName][0].ToString()
                    : string.Empty;
#pragma warning restore CA1416 // Validate platform compatibility
            }
            catch (Exception e)
            {
                _loggerService.WriteError("Exception :: GetProperty occurred :: Details :: " + e.Message + " " +
                    e.InnerException ?? "");
                throw;
            }
        }
    }
}
